package testHelpers;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class DataHelper {

    public String randomStr() {
        return String.valueOf(Math.random() * 1000);
    }

    public int randomInt() {
        return (int) (Math.random() * 1000);
    }

}
