package testHelpers;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class DataHelperTest {
    private final DataHelper dataHelper = new DataHelper();

    @Test
    public void should_return_non_deterministic() {
        assertFalse(dataHelper.randomStr().equals(dataHelper.randomStr()));
    }
}
