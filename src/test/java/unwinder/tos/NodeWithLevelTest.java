package unwinder.tos;

import org.junit.Test;
import tree.Node;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class NodeWithLevelTest {

    @Test
    public void should_persist_constructor_vars() {
        int level = (int) (Math.random() * 100);
        Node node = new Node(String.valueOf(Math.random() * 1000), Collections.emptyList());
        NodeWithLevel levelled = new NodeWithLevel(level, node);
        assertEquals(level, levelled.level);
        assertSame(node, levelled.node);
    }

}
