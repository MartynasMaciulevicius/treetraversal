package unwinder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import tree.Node;
import unwinder.tos.NodeWithLevel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
@RunWith(Parameterized.class)
public class TreeUnwinderTest {

    private final TreeUnwinder treeUnwinder;

    @Parameterized.Parameters(name = "{1}")
    public static Iterable<Object[]> data() {

        return Arrays.asList(
                new Object[]{new RecursiveUnwinder(), RecursiveUnwinder.class},
                new Object[]{new StackUnwinder(), StackUnwinder.class}
        );
    }

    @SuppressWarnings("unused")
    public TreeUnwinderTest(TreeUnwinder treeUnwinder, Class forTestNaming) {
        this.treeUnwinder = treeUnwinder;
    }

    @Test
    public void should_print_one() {
        Node node = newRandomEmptyNode();
        List<NodeWithLevel> out = treeUnwinder.unwind(node).collect(Collectors.toList());
        assertEquals(1, out.size());
        checkNode(node, out.get(0), 0);
    }

    @Test
    public void should_print_child() {
        Node child = newRandomEmptyNode();
        Node node = new Node(random(), Collections.singletonList(child));
        List<NodeWithLevel> out = treeUnwinder.unwind(node).collect(Collectors.toList());
        assertEquals(2, out.size());
        checkNode(node, out.get(0), 0);
        checkNode(child, out.get(1), 1);
    }

    @Test
    public void should_print_children() {
        Node child1 = newRandomEmptyNode();
        Node child2 = newRandomEmptyNode();
        Node child3 = newRandomEmptyNode();
        Node node = new Node(random(), Arrays.asList(child1, child2, child3));
        List<NodeWithLevel> out = treeUnwinder.unwind(node).collect(Collectors.toList());
        assertEquals(4, out.size());
        checkNode(node, out.get(0), 0);
        checkNode(child1, out.get(1), 1);
        checkNode(child2, out.get(2), 1);
        checkNode(child3, out.get(3), 1);
    }

    @Test
    public void should_print_deep_children() {
        Node child1 = newRandomEmptyNode();
        Node deepChild21 = newRandomEmptyNode();
        Node deepChild22 = newRandomEmptyNode();
        Node child2 = new Node(random(), Arrays.asList(deepChild21, deepChild22));
        Node child3 = newRandomEmptyNode();
        Node node = new Node(random(), Arrays.asList(child1, child2, child3));
        List<NodeWithLevel> out = treeUnwinder.unwind(node).collect(Collectors.toList());
        assertEquals(6, out.size());
        checkNode(node, out.get(0), 0);
        checkNode(child1, out.get(1), 1);
        checkNode(child2, out.get(2), 1);
        checkNode(deepChild21, out.get(3), 2);
        checkNode(deepChild22, out.get(4), 2);
        checkNode(child3, out.get(5), 1);
    }

    private void checkNode(Node child1, NodeWithLevel levelled, int level) {
        assertEquals(level, levelled.level);
        assertSame(child1, levelled.node);
    }

    private String random() {
        return String.valueOf(Math.random() * 1000);
    }

    private Node newRandomEmptyNode() {
        return new Node(random(), Collections.emptyList());
    }

}
