package helpers;

import org.junit.Test;
import testHelpers.DataHelper;

import static org.junit.Assert.assertEquals;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class PrependHelperTest {

    private final DataHelper data = new DataHelper();
    private final PrependHelper prependHelper = new PrependHelper();

    @Test
    public void should_return_node_title() {
        String sampleText = data.randomStr();
        String out = prependHelper.prependStr(0, sampleText);
        assertEquals("- " + sampleText + "\n", out);
    }

    @Test
    public void should_return_prepended_node_title() {
        String sampleText = data.randomStr();
        String out = prependHelper.prependStr(1, sampleText);
        assertEquals("  - " + sampleText + "\n", out);
    }

    @Test
    public void should_return_very_prepended_node_title() {
        String sampleText = data.randomStr();
        String out = prependHelper.prependStr(10, sampleText);
        assertEquals("                    - " + sampleText + "\n", out);
    }


}
