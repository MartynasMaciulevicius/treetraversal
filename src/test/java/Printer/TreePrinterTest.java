package Printer;

import helpers.PrependHelper;
import org.junit.Test;
import printer.TreePrinter;
import testHelpers.DataHelper;
import tree.Node;
import unwinder.tos.NodeWithLevel;

import java.util.Collections;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class TreePrinterTest {
    private final PrependHelper prependHelper = new PrependHelper() {
        @Override
        public String prependStr(int level, String text) {
            return String.format("level:%2$d-text:%3$s-random:%1$s\n", prependSeparator, level, text);
        }
    };
    private final TreePrinter treePrinter = new TreePrinter(prependHelper);
    private final DataHelper data = new DataHelper();
    private final String prependSeparator = data.randomStr();

    @Test
    public void should_print_single_node() {
        String text = data.randomStr();
        String out = treePrinter.print(Stream.of(new NodeWithLevel(0, new Node(text, Collections.emptyList()))));
        assertEquals(wrap(0, text), out);
    }

    @Test
    public void should_print_multiple_nodes() {
        String text1 = data.randomStr();
        String text2 = data.randomStr();
        String text3 = data.randomStr();
        String out = treePrinter.print(Stream.of(
                new NodeWithLevel(0, new Node(text1, Collections.emptyList())),
                new NodeWithLevel(0, new Node(text2, Collections.emptyList())),
                new NodeWithLevel(0, new Node(text3, Collections.emptyList()))));
        assertEquals(
                wrap(0, text1)
                        + wrap(0, text2)
                        + wrap(0, text3)
                , out);
    }

    @Test
    public void should_print_multiple_levelled_nodes() {
        String text1 = data.randomStr();
        String text2 = data.randomStr();
        String text3 = data.randomStr();
        int int1 = data.randomInt();
        int int2 = data.randomInt();
        int int3 = data.randomInt();
        String out = treePrinter.print(Stream.of(
                new NodeWithLevel(int1, new Node(text1, Collections.emptyList())),
                new NodeWithLevel(int2, new Node(text2, Collections.emptyList())),
                new NodeWithLevel(int3, new Node(text3, Collections.emptyList()))));
        assertEquals(
                wrap(int1, text1)
                        + wrap(int2, text2)
                        + wrap(int3, text3)
                , out);
    }

    private String wrap(int level, String text) {
        return prependHelper.prependStr(level, text);
    }
}
