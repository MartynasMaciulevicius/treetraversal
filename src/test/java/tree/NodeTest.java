package tree;

import org.junit.Test;
import testHelpers.DataHelper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class NodeTest {
    private final DataHelper data = new DataHelper();

    @Test
    public void should_persist_name() {
        String title = data.randomStr();
        Node node = new Node(title, Collections.emptyList());
        assertEquals(title, node.getText());
    }

    @Test
    public void should_persist_children() {
        List<Node> list = Stream.of(1, 2, 3)
                .map(x -> new Node(String.valueOf(x), Collections.emptyList())).collect(Collectors.toList());
        Node node = new Node("sample text", list);
        assertTrue(node.children().containsAll(list));
    }

}
