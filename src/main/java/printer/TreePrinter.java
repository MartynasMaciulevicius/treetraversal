package printer;

import helpers.PrependHelper;
import unwinder.tos.NodeWithLevel;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class TreePrinter {
    private final PrependHelper prependHelper;

    public TreePrinter(PrependHelper prependHelper) {
        this.prependHelper = prependHelper;
    }

    public String print(Stream<NodeWithLevel> levelledNodes) {
        return levelledNodes
                .map(levelled -> prependHelper.prependStr(levelled.level, levelled.node.getText()))
                .collect(Collectors.joining());
    }
}
