import helpers.PrependHelper;
import printer.TreePrinter;
import tree.Node;
import unwinder.RecursiveUnwinder;
import unwinder.StackUnwinder;
import unwinder.TreeUnwinder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class Main {
    public static void main(String[] args) {
        TreePrinter printer = new TreePrinter(new PrependHelper());
        List<TreeUnwinder> treePrinters = Arrays.asList(new RecursiveUnwinder(), new StackUnwinder());

        String out =
                Stream.of(
                        new Node("Lorem",
                                Arrays.asList(new Node("Ipsum", Collections.emptyList()),
                                        new Node("Dolor", Collections.singletonList(
                                                new Node("Orci", Collections.emptyList())
                                        )))),
                        new Node("Sit", Arrays.asList(
                                new Node("Amet", Collections.emptyList()),
                                new Node("Cons...", Arrays.asList(
                                        new Node("Adipi...", Collections.emptyList()),
                                        new Node("Elit", Collections.emptyList())
                                )),
                                new Node("Vestibulum", Collections.emptyList())
                        )),
                        new Node("Vitae", Collections.emptyList())
                ).map(treePrinters.get((int) (Math.random() * treePrinters.size()))::unwind)
                        .map(printer::print)
                        .collect(Collectors.joining());

        System.out.println(out);
    }
}
