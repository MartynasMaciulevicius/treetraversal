package unwinder;

import tree.Node;
import unwinder.tos.NodeWithLevel;

import java.util.stream.Stream;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public interface TreeUnwinder {
    Stream<NodeWithLevel> unwind(Node node);
}
