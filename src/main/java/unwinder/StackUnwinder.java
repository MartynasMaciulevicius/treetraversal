package unwinder;

import unwinder.tos.NodeWithLevel;
import tree.Node;

import java.util.Collections;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class StackUnwinder implements TreeUnwinder {

    @Override
    public Stream<NodeWithLevel> unwind(Node node) {
        Stack<NodeWithLevel> stack = new Stack<>();
        stack.add(new NodeWithLevel(0, node));
        Stream<NodeWithLevel> builder = Stream.empty();
        do {
            NodeWithLevel current = stack.pop();
            final int level1 = current.level + 1;
            stack.addAll(current.node.children()
                    .stream()
                    .map(child -> new NodeWithLevel(level1, child))
                    .collect(Collectors.collectingAndThen(Collectors.toList(), list -> {
                        Collections.reverse(list);
                        return list;
                    })));
            builder = Stream.concat(builder, Stream.of(current));
        } while (!stack.isEmpty());
        return builder;
    }

}
