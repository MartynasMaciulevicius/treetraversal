package unwinder.tos;

import tree.Node;

/**
 * Created on 2/8/17.
 *
 * @author invertisment
 */
public class NodeWithLevel {

    public final int level;
    public final Node node;

    public NodeWithLevel(int level, Node node) {
        this.level = level;
        this.node = node;
    }
}
