package unwinder;

import unwinder.tos.NodeWithLevel;
import tree.Node;

import java.util.stream.Stream;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class RecursiveUnwinder implements TreeUnwinder {

    @Override
    public Stream<NodeWithLevel> unwind(Node node) {
        return printDeep(0, node);
    }

    private Stream<NodeWithLevel> printDeep(int level, Node node) {
        return Stream.concat(
                Stream.of(new NodeWithLevel(level, node)),
                node.children().stream().flatMap(child -> printDeep(level + 1, child))
        );
    }
}
