package helpers;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class PrependHelper {

    public String prependStr(int level, String text) {
        return Stream
                .iterate("  ", s -> s)
                .limit(level)
                .collect(Collectors.joining()) + "- " + text + "\n";
    }
}
