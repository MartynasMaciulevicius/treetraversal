package tree;

import java.util.List;

/**
 * Created on 2/6/17.
 *
 * @author invertisment
 */
public class Node {

    private final String text;
    private final List<Node> children;

    public Node(String text, List<Node> children) {
        this.text = text;
        this.children = children;
    }

    public List<Node> children() {
        return children;
    }

    public String getText() {
        return text;
    }
}
